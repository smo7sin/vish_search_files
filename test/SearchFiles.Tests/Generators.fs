﻿[<AutoOpen>]
module Generators

open System
open FsCheck
open FileExplorer

[<AutoOpen>]
module FileContent = 
    let SearchWord = "test"
    
    let private genSpace = Arb.from<char> |> Arb.filter (Char.IsWhiteSpace) |> Arb.toGen |> Gen.map string
    
    let private genWord = gen {
        let! word = Arb.generate<string>
        let! space = genSpace
    
        return word + space
    }

    let private genContent = 
        Gen.optionOf (
        gen {
            let! words = Gen.listOf (Gen.oneof [genWord; Gen.constant SearchWord])
            let! seperator = genSpace
            let content = words |> List.fold (fun state item -> state + seperator + item) String.Empty
            return content
        }) |> Gen.map (fun x -> FileContent x)
    
    type FileContentGenerator =
        static member FileContent () = Arb.fromGen genContent
    

let private genName = Arb.generate<string>

[<AutoOpen>]
module FileInfo = 
    
    let genFileInfo = gen {
        let! name = genName
        let! content = Arb.generate<FileContent>
        return createFileInfo name content
    }

    type FileInfoGenerator =
        static member FileInfo () = Arb.fromGen genFileInfo

[<AutoOpen>]
module FileTree =

    let private genFile () = Arb.generate<FileInfo> |> Gen.map File
   
    let private fileTree =
        let genNoTree () = gen {
            let! name = genName
            let! files = Gen.listOf (genFile ())
            return createDir name files
        }
        let rec fileTree' s = gen {
            
            match s with
            | 0 -> return! genNoTree ()
            | n when n > 0 -> 
                let! genDirs = Gen.listOf (fileTree' (n/2))
                let! genFiles = Gen.listOf (genFile ())
                let! name = genName
                return createDir name (Seq.append genDirs genFiles)
            | _ -> return invalidArg (nameof s) "Only positive arguments are allowed"
        }
        Gen.sized fileTree'

    type FileTreeGenerator =
        static member FileTree () = Arb.fromGen fileTree