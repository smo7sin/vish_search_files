﻿module FileInfoTests

open FSharp.Control
open Xunit
open FsCheck
open FsCheck.Xunit
open FileExplorer


[<Property(Arbitrary=[| typeof<FileInfoGenerator>; typeof<FileContentGenerator> |])>]   
let ``Search asyncronounsly file for test word`` (file: FileInfo) = 
    async {
        let! searchResult = file |> FileInfo.isWordInContentAsync SearchWord
        let! (FileContent maybeContent) = FileInfo.getContent file
        match maybeContent with
        | Some content -> 
            return searchResult ==> content.Contains SearchWord
        | None -> 
            return searchResult ==> false
    } |> Async.RunSynchronously
    