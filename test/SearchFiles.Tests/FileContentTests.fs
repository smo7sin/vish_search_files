﻿
module FileContentTests

open FSharp.Control
open Xunit
open FsCheck
open FsCheck.Xunit
open FileExplorer


let contentcases1 word : obj[] seq = 
    seq {
        ("sdfdsf sddsf sdfdsfs test\tddd", true)
        ("sdfdsf sddsf sdfdsfs TeSt ddd", true)
        ("sdfdsf sddsf sdfdsfs test\nNewline starts", true)
        ("sdfdsf sddsf sdfdsfs test", true)
        ("sdfdsf sddsf sdfdsf_test", false)
        ("sdfdsf sddsf sdfdsf test. sdf sdf", true)
        ("sdfdsf sddsf sdfdsf test.",  true)
    }
    |> Seq.map (fun (x, z) -> [| FileContent.create x; word; z|] )

let contentData: obj[] seq = 
    seq {
        let word = FileContent.SearchWord
        yield! contentcases1 word
        yield [| FileContent.None; "test"; false |]
    } 

[<Theory>]
[<MemberData (nameof contentData)>]
let ``Search file for a word test`` (content: FileContent) (word: string) expected = 
    let actual = content |> FileContent.containsWord word
    Assert.Equal (expected, actual)
    
[<Property(Arbitrary=[| typeof<FileContentGenerator> |])>]   
let ``Search file content for test word`` (fileContent) = 
    let (FileContent maybeContent) = fileContent
    match maybeContent with
    | Some content -> 
        fileContent |> FileContent.containsWord SearchWord ==> content.Contains SearchWord
    | None -> 
        fileContent |> FileContent.containsWord SearchWord ==> false
    