﻿[<AutoOpen>]
module Helpers

open FSharpx
open FileExplorer

type FileMap = Map<string, FileInfo list>
type DirMap = Map<string, string list>

let createFileInfo path content =
    { Path = FilePath path; ContentAsync = lazy (Async.returnM content) }

let createFile path content = File (createFileInfo path content)

let createDir path descendants =
    Directory { Path = DirectoryPath path; Descendants = descendants }
        
module FileTree = 
    let flatFileTree tree =
        let rec flatFileTree' tree = seq {
            match tree with 
            | File inf -> yield File inf
            | Directory dir -> yield Directory dir; yield! dir.Descendants |> Seq.map flatFileTree' |> Seq.collect id
        }
        flatFileTree' tree
