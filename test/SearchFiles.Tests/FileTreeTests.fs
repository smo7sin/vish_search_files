module FileTreeTests

open FSharp.Control
open FSharpx
open Xunit

open FsCheck.Xunit
open FileExplorer

let pathsData : obj [] [] = [|
    [| (DirMap [ ("a", ["aa"; "ab"]); ("aa", ["aaa"; "aab"]); ("ab", ["aba"])]);
        FileMap Seq.empty; 6 |];
   [| (DirMap [ ("a", List.empty)]); FileMap Seq.empty; 1 |];
   [| (DirMap [ ("a", List.empty)]); FileMap [("a", [(createFileInfo "a1" (FileContent.create "test content"))])]; 2 |];
|]
 
[<Theory>]
[<MemberData (nameof pathsData)>]
let ``Build file tree from a directory test`` (paths: DirMap) (files: FileMap) expectedCount =
        
    let getDirectories: GetDirectoryPaths = 
        fun (DirectoryPath key) ->
            let mayBeValues = paths |> Map.tryFind key
            match mayBeValues with
            | Some values -> values |> Seq.map DirectoryPath
            | None -> Seq.empty

    let getFiles: GetFiles = 
        fun (DirectoryPath key) ->
            let mayBeValues = files |> Map.tryFind key
            match mayBeValues with
            | Some values -> values
            | None -> Seq.empty

    let actual = Result.get (FileTree.fromDirectory (DirectoryPath "a") getDirectories getFiles)

    Assert.Equal (expectedCount, FileTree.flatFileTree actual |> Seq.length)

[<Property(Arbitrary = [| typeof<FileTreeGenerator>; typeof<FileInfoGenerator>; typeof<FileContentGenerator> |])>]
let ``search arbitrary file tree for a word`` tree =
    async {
        let! expectedFiles = 
            tree 
            |> FileTree.flatenToFiles 
            |> Seq.map FileInfo.getContent 
            |> AsyncSeq.ofSeqAsync 
            |> AsyncSeq.map (fun (FileContent x) -> x )
            |> AsyncSeq.choose id
            |> AsyncSeq.filter (fun content ->  content |> String.contains SearchWord )
            |> AsyncSeq.toListAsync

        let! files = FileTree.searchFilesAsync tree FileContent.SearchWord |> AsyncSeq.toListAsync
        let expectedCount = expectedFiles |> List.length
        let actualCount = files |> List.length
        Assert.Equal (actualCount,  expectedCount)
    } |> Async.RunSynchronously