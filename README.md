## Introduction

This application search text files by given a word term criteria. 

# Installing dotnet sdk on Ubuntu 20.04

Please, open bash terminal and copy paste the following command to add microsoft package sources 

```
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb
rm packages-microsoft-prod.deb

```

# Install the SDK

To install the .NET SDK, run the following commands:
```
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-6.0
```

# Install the Runtime

The ASP.NET Core Runtime allows you to run apps that were made with .NET that didn't provide the runtime. The following commands install the ASP.NET Core Runtime, which is the most compatible runtime for .NET. In your terminal, run the following commands:

```
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-6.0
```

To confirm donet is installed properly, run the following command:
```
dotnet --info
``` 
It should output dotnet version installed

## Building the project
From the root directory of the repository where the sln file is residing; run the following command.
```
dotnet build
``` 

## Running the test

From the root directory where the sln file is residing; run the following command.
```
dotnet test
``` 

## Running the program

There are two ways to run the program. If you already compiled the project then you can run the following command:

```
dotnet ./src/SearchFiles.Main/bin/Debug/net6.0/SearchFiles.Main.dll
```

Otherwise you can also run the following command 

```
 dotnet run --project ./src/SearchFiles.Main/
```