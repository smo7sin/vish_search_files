module FileStore

open System.IO
open FSharpx
open FileExplorer

[<Literal>]
let fileExtension = "txt"

module DirectoryPath =
    let fromFileSystem path =
        if Directory.Exists path then
            Ok (DirectoryPath path)
        else
            Error (exn $"Directory path '{path}' not found")

module FileInfo =
    //Assuming all files are readable
    let fromPath (FilePath path) =
        let tryReadAllTextAsync path' = async {
            try
                let! text = File.ReadAllTextAsync(path') |> Async.AwaitTask
                return FileContent.create text
            with
            | _ -> return FileContent.None // Ok to swallow exception. ideal to log.
        } 
            
        { Path = FilePath path; ContentAsync = lazy ( tryReadAllTextAsync path )  }
   
    let fromString text =
        fromPath (FilePath text)

module DirectoryInfo =
    let getTextFilePaths path = Directory.EnumerateFiles (path, $"*.{fileExtension}")  
    let getFilePaths: GetFiles  = function
        | DirectoryPath path ->
            let getPaths = getTextFilePaths >> Seq.map FileInfo.fromString
            let tryGetFiles = Result.protect getPaths path
            
            tryGetFiles |> Result.defaultValue Seq.empty

    let getDirectoryPaths: GetDirectoryPaths = function
        | DirectoryPath path ->
            let getPaths p = Directory.EnumerateDirectories path |> Seq.map DirectoryPath
            let tryGetDirs = Result.protect getPaths path
            tryGetDirs |> Result.defaultValue Seq.empty

module FileTree =
    let fromFileSystem path = Result.result {
        let! validPath = DirectoryPath.fromFileSystem path 
        let! tree = FileTree.fromDirectory validPath DirectoryInfo.getDirectoryPaths DirectoryInfo.getFilePaths
        return tree
    }