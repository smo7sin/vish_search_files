﻿module Program
open FSharp.Control
open FSharpx
open FileExplorer
open FileStore

let printFilePath { Path = FilePath path; ContentAsync = _ } = 
    stdout.WriteLine path

let app path word = async {
        let result = FileTree.fromFileSystem path
        match result with
        | Ok fileTree -> 
            let files = FileTree.searchFilesAsync fileTree word
            do! files |> AsyncSeq.iter printFilePath

            let! len = files |> AsyncSeq.length
            if len < 1 then stdout.WriteLine $"No files found with search term: {word}"
        | Error e -> stdout.WriteLine ("Error occured: " + e.Message)
    }

[<EntryPoint>]
let main _ = 
    stdout.WriteLine "Enter a file path"
    let dir = stdin.ReadLine ()
    stdout.WriteLine "Enter a word to search in text files"
    let word = stdin.ReadLine ()

    app dir word |> Async.RunSynchronously
    0