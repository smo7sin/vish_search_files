﻿
namespace FileExplorer 

open FSharpx

type FileInfo = {
    Path: FilePath
    ContentAsync: FileContent Async Lazy
}

module FileInfo = 

    let getContent file = 
        let content = Lazy.force file.ContentAsync
        content

    let isWordInContentAsync word file = async {
        let! content = getContent file
        return content |> FileContent.containsWord word
    }


