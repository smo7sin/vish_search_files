﻿
namespace FileExplorer 


open System
open FSharpx
open FSharpx.Text 

type RegexOptions = System.Text.RegularExpressions.RegexOptions

type FileContent = FileContent of string option

module FileContent =

    let create text = FileContent (Some text)

    let Empty = create String.Empty
    let None = FileContent None

    let get (FileContent content) = content
    let getOrDefault = get >> Option.defaultValue String.Empty

    let containsWord (word: string) (FileContent mayBeContent) = 
        Option.maybe {
            let! content = mayBeContent
            let! found = (Regex.tryMatchWithOptions RegexOptions.IgnoreCase $"\W+{word}(\W+|$)" content)

            return found
        } |> Option.isSome
