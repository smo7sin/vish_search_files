﻿
namespace FileExplorer 

open FSharp.Control
open FSharpx

type FileTree =
    | File of FileInfo
    | Directory of DirectoryInfo
and DirectoryInfo = {
    Path: DirectoryPath
    Descendants: FileTree seq
}

type GetFiles = DirectoryPath -> FileInfo seq
type GetDirectoryPaths = DirectoryPath -> DirectoryPath seq
type BuildFileTree = DirectoryPath -> GetDirectoryPaths -> GetFiles -> Result<FileTree, exn>

module FileTree =
    
    let fromDirectory : BuildFileTree =
        fun path getDirectoryPaths getFiles ->
        let rec buildFileTree' path =        
            let files = getFiles path |> Seq.map File
            let dirs = getDirectoryPaths path |> Seq.map buildFileTree'
            let descendants = files |> Seq.append dirs 
            Directory { Path = path; Descendants = descendants }
        Result.protect buildFileTree' path
    
    let flatenToFiles tree =
        let rec flatenToFiles' tree = seq {
            match tree with 
            | File inf -> yield inf
            | Directory dir -> yield! dir.Descendants |> Seq.map flatenToFiles' |> Seq.collect id
        }
        flatenToFiles' tree

    let searchFilesAsync tree word = 
        flatenToFiles tree 
        |> AsyncSeq.ofSeq 
        |> AsyncSeq.filterAsync (FileInfo.isWordInContentAsync word)